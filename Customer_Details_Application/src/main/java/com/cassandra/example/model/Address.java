package com.cassandra.example.model;

import lombok.Data;


import org.hibernate.validator.constraints.Range;
import org.springframework.data.cassandra.core.mapping.UserDefinedType;

@Data
@UserDefinedType("address")
public class Address {

	private String addressLine1;

	private String addressLine2;

	private String city;

	@Range(min = 00001,max = 99999, message = "zip must be five")
	private int zip;

	@Range(min=01,max = 99,message = "state must be two character")
	private String state;


}
