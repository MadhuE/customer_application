package com.cassandra.example.model;

import lombok.Data;

import org.hibernate.validator.constraints.Range;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Data
@Table("customer")
public class Customer {

	@PrimaryKey
	private int billingAccountNumber;

	private String firstName;

	private String lastName;

	@Range(max = 10)
	private Long phoneNumber;

	private String emailId;
	

	@Column("address")
	private Address address;

	

}
