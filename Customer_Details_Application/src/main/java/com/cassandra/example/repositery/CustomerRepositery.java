package com.cassandra.example.repositery;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;
import com.cassandra.example.model.Customer;

@Repository
public interface CustomerRepositery extends CassandraRepository<Customer, Integer> {

}
