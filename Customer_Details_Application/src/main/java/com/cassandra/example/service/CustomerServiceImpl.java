package com.cassandra.example.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cassandra.example.model.Customer;
import com.cassandra.example.repositery.CustomerRepositery;

import io.netty.util.internal.ThreadLocalRandom;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepositery repo;

	@Override
	public boolean createCustomer(Customer customer) {

		customer.setBillingAccountNumber(ThreadLocalRandom.current().nextInt(999999999));
		Customer create = repo.save(customer);
		return true;

	}

	@Override
	public Customer updateCustomer(Customer customer, int billingAccountNumber) {
		Optional<Customer> update=repo.findById(billingAccountNumber);
		
		Customer _customer=update.get();
		_customer.setAddress(customer.getAddress());
		_customer.setEmailId(customer.getEmailId());
    	return repo.save(_customer);
	}

	@Override
	public Customer getCustomer(int billingAccountNumber) {

		Optional<Customer> find = repo.findById(billingAccountNumber);
		return find.orElse(null);
	}

	@Override
	public boolean deleteCustomer(int billingAccountNumber) {
		Optional<Customer> customer=repo.findById(billingAccountNumber);
		if(customer.isPresent()) {
			repo.deleteById(billingAccountNumber);
			return true;
		}
		return false;
	}

}
