package com.cassandra.example.service;

import com.cassandra.example.model.Customer;

public interface CustomerService {

	boolean createCustomer(Customer customer);

	Customer updateCustomer(Customer customer, int billingAccountNumber);

	Customer getCustomer(int billingAccountNumber);

	boolean deleteCustomer(int billingAccountNumber);
}
