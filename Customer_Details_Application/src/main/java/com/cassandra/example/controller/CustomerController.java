package com.cassandra.example.controller;


import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import com.cassandra.example.model.Customer;
import com.cassandra.example.service.CustomerService;

@RestController
@RequestMapping("/api")
public class CustomerController {

	Logger logger = LoggerFactory.getLogger(CustomerController.class);

	private CustomerService service;

	public CustomerController(CustomerService service) {
		this.service = service;
	}

	@PostMapping("/create")
	public ResponseEntity<String> createCustomer(@Valid @RequestHeader int conversationId,
			@RequestBody Customer customer) throws MethodArgumentNotValidException {
		logger.debug("***  createCustomer()- Execution started");
		try {
			boolean save = service.createCustomer(customer);
			if (save) {
				logger.info("****createdCustomer()- customer saved****");
				return new ResponseEntity<>("created Successful", HttpStatus.OK);
			}
		} catch (Exception e) {
			logger.error("Exception occurred :" + e.getMessage());
		}
		logger.error("********createCustomer()- customer creating failed *****");
		return new ResponseEntity<String>("failed to create", HttpStatus.BAD_REQUEST);
	}

	@PutMapping("/update/{billingAccountNumber}")
	public ResponseEntity<String> updateCustomer(@RequestBody Customer customer,
			@PathVariable int billingAccountNumber) {
		logger.debug("**** updateCustomer()customer updation started**");
		Customer save = service.updateCustomer(customer, billingAccountNumber);
		logger.info("**updateCustomer()-customer updated successful");
		return new ResponseEntity<String>("created successful", HttpStatus.NO_CONTENT);

	}

	@GetMapping("/find/{billingAccountNumber}")
	public ResponseEntity<Customer> getCustomer(@PathVariable int billingAccountNumber) {
		logger.debug("**** getCustomer()customer findById started**");

		Customer save = service.getCustomer(billingAccountNumber);
		logger.info("**getCustomer()-customer response body  successful");
		return new ResponseEntity<>(save, HttpStatus.OK);
	}

	@DeleteMapping("/delete/{billingAccountNumber}")
	public ResponseEntity<String> deleteCustomer(@PathVariable int billingAccountNumber) {

		logger.debug("***  createCustomer()- Execution started");
		try {
			boolean delete = service.deleteCustomer(billingAccountNumber);
			if (delete) {
				logger.info("****createdCustomer()- customer saved****");
				return new ResponseEntity<>("deleted", HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {

			logger.error("Exception occurred :" + e.getMessage());
		}
		logger.error("********createCustomer()- customer creating failed *****");
		return new ResponseEntity<>("failed to delete", HttpStatus.NOT_FOUND);
	}
}
